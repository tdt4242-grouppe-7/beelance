function alert(message, type) {
    const alert = document.getElementsByClassName('alert-container')[0];
    alert.style = "display:flex;";
    alert.getElementsByTagName('div')[0].innerHTML = message;
    setTimeout(function () {
        alert.style = "display:none;";
    }, 2000)
}

function updateComment(data, callback = null) {
    const xhr1 = new XMLHttpRequest();
    xhr1.open(data.type, data.url, true);
    xhr1.setRequestHeader('X-CSRFToken', data.token);
    xhr1.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr1.onreadystatechange = function () {
        if (xhr1.readyState === 4 && xhr1.status === 200) {
            const response = JSON.parse(xhr1.response);
            if (callback) {
                callback(response);
            }
        }
    };
    xhr1.send(JSON.stringify(data));
}

function swap(e, updatedValue = null) {
    e = e || window.event;
    const target = e.target || e.srcElement;
    let node = target.parentNode.parentNode.parentNode.parentNode;
    let comment_content = node.childNodes[3].childNodes[3].childNodes[1];
    let textArea = node.childNodes[3].childNodes[3].childNodes[3];
    let editButton = node.childNodes[1].childNodes[1].childNodes[1].childNodes[1];
    let submitButton = node.childNodes[1].childNodes[1].childNodes[1].childNodes[5];
    if (editButton.innerHTML === "Edit") {
        comment_content.style = "display: none";
        textArea.style = "display: block";
        editButton.innerHTML = "Cancel";
        submitButton.style = "display: block";
    } else {
        if (updatedValue) {
            comment_content.innerHTML = updatedValue
        }
        comment_content.style = "display: block";
        textArea.style = "display: none";
        editButton.innerHTML = "Edit";
        submitButton.style = "display: none";
    }
}

function editComment(projectID, commentID, token) {
    const clickEvent = event;
    const textArea = (event.target.parentNode.parentNode.parentNode.parentNode).getElementsByTagName('textarea')[0];
    const data = {
        comment: textArea.value,
        url: "/projects/" + projectID + "/comments/" + commentID,
        token: token,
        type: 'PUT',
    };
    updateComment(data, function (response) {
        swap(clickEvent, textArea.value);
        alert(response.msg, "succsess")
    })
}

function deleteComment(projectID, commentID, token) {
    const data = {
        url: "/projects/" + projectID + "/comments/" + commentID,
        token: token,
        type: 'DELETE'
    };

    const element = event.target.parentNode.parentNode;
    updateComment(data, function (response) {
        element.remove();
        alert(response.msg, "succsess")
    });
}
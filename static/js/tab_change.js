$(() => {
    function updateProjects(addToPage = 0) {
        const search = $('#search_box').val();
        const page = parseInt($('#page-number').val()) + addToPage;
        const category = ($('.list-group-item.list-group-item-action.active.show').html()).replace(/\s+/, "");
        window.location.href = "/projects/?search=" + search + "&page=" + page + "&category=" + category;
    }

    $('#search_submit').click(() => {
        event.preventDefault();
        updateProjects();
    });

    $('#btn-next').click(() => {
        event.preventDefault();
        updateProjects(1);
    });

    $('#btn-prev').click(() => {
        event.preventDefault();
        updateProjects(-1);
    });


    $('#project-toggle a[data-toggle="list"]').on('shown.bs.tab', (e) => {
        e.preventDefault();
        $('#' + e.relatedTarget.id).removeClass('active') // previous active tab
        updateProjects();
    });

});
from django.urls import path
from .views import views
from .views import new_project
from .views import comments
from .views import tasks

urlpatterns = [
    path('', views.projects, name='projects'),
    path('new/', new_project.new_project, name='new_project'),
    path('<project_id>/', views.project_view, name='project_view'),
    path('<project_id>/comments', comments.create_comment, name='comment_section'),
    path('<project_id>/comments/<comment_id>', comments.manage_comments, name='comment_id'),
    path('<project_id>/tasks/<task_id>/', tasks.task_view, name='task_view'),
    path('<project_id>/tasks/<task_id>/upload/', tasks.upload_file_to_task, name='upload_file_to_task'),
    path('<project_id>/tasks/<task_id>/permissions/', tasks.task_permissions, name='task_permissions'),
    path('delete_file/<file_id>', views.delete_file, name='delete_file'),
]

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, JsonResponse
import json

from django.shortcuts import redirect

from projects.models import Comment


@login_required
def create_comment(request, project_id):
    if request.POST and project_id:
        data = request.POST
        try:
            Comment.objects.create(project_id=int(project_id), comment=data['comment'],
                                   created_by=request.user.profile)
            return redirect('/projects/' + project_id + '/#comment-section')
        except Exception:
            raise Http404("Could not create comment")
    else:
        raise Http404("Request must be post and include project-id")


@login_required
def manage_comments(request, project_id, comment_id):
    if request.method == "PUT":
        return edit_comment(request, comment_id)
    elif request.method == "DELETE":
        return delete_comment(request, comment_id)
    else:
        raise Http404("Valid choices are DELETE or PUT")


def delete_comment(request, comment_id):
    try:
        Comment.objects.get(pk=comment_id, created_by_id=request.user.pk).delete()
        return JsonResponse({'msg': 'Comment deleted'})
    except ObjectDoesNotExist as e:
        raise Http404("Coument with id " + comment_id + " does not exist")


def edit_comment(request, comment_id):
    comment_text = json.loads(request.body).get('comment', None)
    if not comment_text:
        raise Http404("Comment must be provided")
    try:
        comment = Comment.objects.get(pk=comment_id, created_by_id=request.user.pk)
        comment.comment = comment_text
        comment.save()
        return JsonResponse({'msg': 'Comment updated', 'comment': comment.comment})
    except ObjectDoesNotExist as e:
        print("exception", e)
        raise Http404("Could not edit comment")

from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from projects.models import Comment
from ..models import Project, Task, TaskOffer, ProjectCategory, TaskFile
from ..forms import ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm
from django.core.paginator import Paginator
from functools import reduce


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def projects(request):
    search_term = request.GET.get("search", None)
    project_categories = ProjectCategory.objects.all()
    category = request.GET.get('category', project_categories[0])
    projects = Project.objects.filter(category__name=category,
                                      status='o') if not search_term else Project.objects.filter(
        title__icontains=search_term,
        status='o',
        category__name=category)
    paginator = Paginator(projects, 10)
    page = request.GET.get('page', 1)
    projects = paginator.get_page(page)
    print(projects.object_list)
    return render(request,
                  'projects/projects.html',
                  {
                      'projects': projects,
                      'project_categories': project_categories,
                      'category': category,
                      'search_term': search_term
                  })


def fill_offer_response(request, project):
    """" Validates the offer response """
    instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
    offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
    if offer_response_form.is_valid():
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == 'a':
            offer_response.task.read.add(offer_response.offerer)
            offer_response.task.write.add(offer_response.offerer)
            project = offer_response.task.project
            project.participants.add(offer_response.offerer)
        offer_response.save()

def fill_status_change(request, project):
    """ Validates the status change action """
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()

def fill_offer_submit(request, project):
    """ Validates the offer submitting """
    task_offer_form = TaskOfferForm(request.POST)
    if task_offer_form.is_valid():
        task_offer = task_offer_form.save(commit=False)
        task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
        task_offer.offerer = request.user.profile
        task_offer.save()

def project_view(request, project_id):
    """ Renders the projet view """
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()

    #Adds data to the mandatory fields
    render_dict = {
        'comments': Comment.get_project_comments(project_id),
        'project': project,
        'tasks': tasks,
        'total_budget': reduce((lambda x,y: x+y.budget), tasks, 0)
    }

    #Adds data to offer response and statuse change
    if request.user == project.user.user:
        if request.method == 'POST':
            if 'offer_response' in request.POST:
                fill_offer_response(request, project)

            if 'status_change' in request.POST:
                fill_status_change(request, project)

        render_dict['offer_response_form'] = TaskOfferResponseForm()
        render_dict['status_form'] = ProjectStatusForm(initial={'status': project.status})

    #Adds data to offer submit
    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            fill_offer_submit(request, project)

        render_dict['task_offer_form'] = TaskOfferForm()

    return render(request, 'projects/project_view.html', render_dict)


def is_project_owner(user, project):
    return user == project.user.user

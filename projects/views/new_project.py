from smtplib import SMTPAuthenticationError
from django.shortcuts import render, redirect
from ..models import Task
from ..forms import ProjectForm
from django.contrib.auth.decorators import login_required
from user.models import Profile
from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.contrib import messages


@login_required
def new_project(request):
    """ Creates a new project with related tasks. """
    if request.method == 'POST':
        form = ProjectForm(request.POST, request=request)
        if form.is_valid():
            project = form.save()
            send_new_project_mail(project, request)
            create_tasks(project, request.POST)
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm(request=request)
    return render(request, 'projects/new_project.html', {'form': form})


def send_new_project_mail(project, request):
    """ Sends a email to every person subscribing to the given project category. """
    people = Profile.objects.filter(categories__id=project.category.id).exclude(user__email__isnull=True)

    # List of email addresses based on users with the emailfield set
    send_mail_to = list(map(lambda person: person.user.email, people))

    # Context used to send the email
    title = "New Project: " + project.title
    main_content = "A new project you might be interested in was created and can be viwed at "
    url = get_current_site(request).domain + '/projects/' + str(project.id)

    try:
        with mail.get_connection() as connection:
            mail.EmailMessage(title, main_content + url, "Beelancer", send_mail_to,
                              connection=connection).send()
    except SMTPAuthenticationError:
        messages.error(request, "Sending of email failed.")


def create_tasks(project, data):
    """ Creates a set of tasks for the given project, based on the provided data. """
    task_title = data.getlist('task_title')
    task_description = data.getlist('task_description')
    task_budget = data.getlist('task_budget')
    for i in range(len(task_title)):
        Task.objects.create(
            title=task_title[i],
            description=task_description[i],
            budget=task_budget[i],
            project=project,
        )

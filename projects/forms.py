from django import forms
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect

from .models import Project, TaskFile, TaskOffer, Delivery, ProjectCategory, Team
from django.contrib.auth.models import User
from user.models import Profile


class ProjectForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    category_id = forms.ModelChoiceField(queryset=ProjectCategory.objects.all())

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ProjectForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Project
        fields = ('title', 'description', 'category_id')

    def save(self, commit=True):
        project = super(ProjectForm, self).save(commit=False)
        project.user = self.request.user.profile
        project.category = ProjectCategory.objects.get(id=self.request.POST.get('category_id'))
        project.save()
        return project

    def clean(self):
        cd = self.cleaned_data
        if not ProjectCategory.objects.filter(id=self.request.POST.get('category_id')).exists():
            raise ValidationError("The category does not exist")
        if not self.request.user.profile:
            raise ValidationError("Invalid user")
        return cd


class TaskFileForm(forms.ModelForm):
    file = forms.FileField()

    class Meta:
        model = TaskFile
        fields = ('file',)


class ProjectStatusForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('status',)


class TaskOfferForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    price = forms.NumberInput()

    class Meta:
        model = TaskOffer
        fields = ('title', 'description', 'price',)


class TaskOfferResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = TaskOffer
        fields = ('status', 'feedback')


class TaskDeliveryResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = Delivery
        fields = ('status', 'feedback')


PERMISSION_CHOICES = (
    ('Read', 'Read'),
    ('Write', 'Write'),
    ('Modify', 'Modify'),
)


class TaskPermissionForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    permission = forms.ChoiceField(choices=PERMISSION_CHOICES)


class DeliveryForm(forms.ModelForm):
    comment = forms.Textarea()
    file = forms.FileField()

    class Meta:
        model = Delivery
        fields = ('comment', 'file')


class TeamForm(forms.ModelForm):
    name = forms.CharField(max_length=50)

    class Meta:
        model = Team
        fields = ('name',)


class TeamAddForm(forms.ModelForm):
    members = forms.ModelMultipleChoiceField(queryset=Profile.objects.all(), label='Members with read')

    class Meta:
        model = Team
        fields = ('members',)

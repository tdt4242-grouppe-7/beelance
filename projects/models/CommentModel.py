from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from user.models import Profile


class Comment(models.Model):
    class Meta:
        ''' Configure the name displayed in the admin panel '''
        verbose_name = "Project comment"
        verbose_name_plural = "Project comments"

    comment = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(Profile, on_delete=models.PROTECT)
    project = models.ForeignKey('project', on_delete=models.PROTECT)

    def __str__(self):
        return self.comment

    @staticmethod
    def get_project_comments(project_id):
        try:
            return Comment.objects.select_related('created_by').filter(project=project_id).order_by("-created_at")
        except ObjectDoesNotExist:
            return []

from django.test import TestCase, Client
from ..views import project_view, get_user_task_permissions
from ..models.models import ProjectCategory, Project, Task, TaskOffer
from user.models import Profile

from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase


class TestGetUserTaskPermissions(TestCase):
    def setUp(self):
        self.pCategory = ProjectCategory.objects.create(pk=1)
        self.user = User.objects.create_user(
            pk=1,
            username='admin',
            password='qwerty123')
        self.other_user = User.objects.create_user(
            pk=2,
            username="harrypotter",
            password="qwerty123")
        self.profile = Profile.objects.get(user=self.user)
        self.other_profile = Profile.objects.get(user=self.other_user)
        self.project = Project.objects.create(
            pk=1,
            user=self.profile,
            category=self.pCategory)
        self.task = Task.objects.create(project=self.project)
        self.other_task = Task.objects.create(project=self.project)

    def test_user_owner(self):
        self.assertEquals(get_user_task_permissions(self.user, self.task),
                          {
                              'write': True,
                              'read': True,
                              'modify': True,
                              'owner': True,
                              'upload': True,
                          })

    def test_no_owner(self):
        self.assertEquals(get_user_task_permissions(self.other_user, self.task),
                          {
                              'write': False,
                              'read': False,
                              'modify': False,
                              'owner': False,
                              'view_task': False,
                              'upload': False,
                          })

    def test_accepted_user(self):
        self.taskOffer = TaskOffer.objects.create(
            task=self.other_task,
            offerer=self.other_profile,
            status='a')
        self.assertEquals(get_user_task_permissions(self.other_user, self.other_task),
                          {
                              'write': True,
                              'read': True,
                              'modify': True,
                              'owner': False,
                              'upload': True,
                          })


class Test_project_view(TestCase):
    print("Testing project_view")

    def setUp(self):
        self.factory = RequestFactory()

        self.pCategory = ProjectCategory.objects.create(pk=1)

        self.user = User.objects.create_user(
            pk=1,
            username='admin',
            password='qwerty123')
        self.other_user = User.objects.create_user(
            pk=2,
            username="harrypotter",
            password="qwerty123")
        self.profile = Profile.objects.get(user=self.user)
        self.other_profile = Profile.objects.get(user=self.other_user)

        self.project = Project.objects.create(
            pk=1,
            user=self.profile,
            category=self.pCategory)

        self.task = Task.objects.create(project=self.project)
        self.other_task = Task.objects.create(project=self.project)

        self.task_offer = TaskOffer(
            task=self.task,
            offerer=self.profile)
        self.task_offer.save()

    def test_offer_response(self):
        data = {
            'offer_response': '',
            'taskofferid': 1,
            'status': 'a',
            'feedback': 'test'
        }
        request = self.factory.post('/projects/', data)
        request.user = self.user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

    def test_status_change(self):
        data = {
            'status_change': '',
            'status': self.project.status
        }
        request = self.factory.post('/projects/', data)
        request.user = self.user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

    def test_offer_submit(self):
        data = {
            'offer_submit': '',
            'title': 'test',
            'description': 'test',
            'price': 1,
            'taskvalue': 1
        }
        request = self.factory.post('/projects', data)
        request.user = self.other_user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

from django.test import Client
from django.urls import reverse

from ..models.models import ProjectCategory, Project, Task, TaskOffer
from user.models import Profile

from django.contrib.auth.models import User
from django.test import TestCase


class TestCommentSection(TestCase):
    def setUp(self):
        self.pCategory = ProjectCategory.objects.create(pk=1)
        self.user = User.objects.create_user(
            pk=1,
            username='admin',
            password='qwerty123')
        self.other_user = User.objects.create_user(
            pk=2,
            username="harrypotter",
            password="qwerty123")
        self.profile = Profile.objects.get(user=self.user)
        self.other_profile = Profile.objects.get(user=self.other_user)
        self.project = Project.objects.create(
            pk=1,
            user=self.profile,
            category=self.pCategory)
        self.task = Task.objects.create(project=self.project)
        self.other_task = Task.objects.create(project=self.project)

    def test_access_without_login(self):
        """ Try to create a new comment without being authenticated """
        self.c = Client()
        response = self.c.post(reverse('comment_section', args=(1,)), follow=False)
        self.assertIsNot(response.status_code, 302)

    def test_access_with_login_and_invalid_value(self):
        """ Try to create a new comment with invalid value """
        self.c = Client()
        self.c.login(username='admin', password='qwerty123')
        response = self.c.post(reverse('comment_section', args=(0,)), follow=False)
        self.assertEquals(response.status_code, 404)

    def test_access_with_login_and_valid_value(self):
        """ Try to create a new post with a valid value """
        self.c = Client()
        self.c.login(username='admin', password='qwerty123')
        response = self.c.post(reverse('comment_section', args=(1,)), {'comment': 'hello'}, follow=False)
        self.assertEquals(response.status_code, 302)

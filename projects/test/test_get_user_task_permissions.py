from django.test import TestCase, Client
from ..views import project_view, get_user_task_permissions
from ..models.models import ProjectCategory, Project, Task, TaskOffer
from user.models import Profile

from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase


class TestGetUserTaskPermissions(TestCase):
    def setUp(self):
        self.pCategory = ProjectCategory.objects.create(pk=1)
        self.user = User.objects.create_user(
            pk=1,
            username='admin',
            password='qwerty123')
        self.other_user = User.objects.create_user(
            pk=2,
            username="harrypotter",
            password="qwerty123")
        self.profile = Profile.objects.get(user=self.user)
        self.other_profile = Profile.objects.get(user=self.other_user)
        self.project = Project.objects.create(
            pk=1,
            user=self.profile,
            category=self.pCategory)
        self.task = Task.objects.create(project=self.project)
        self.other_task = Task.objects.create(project=self.project)

    def test_user_owner(self):
        self.assertEquals(get_user_task_permissions(self.user, self.task),
                          {
                              'write': True,
                              'read': True,
                              'modify': True,
                              'owner': True,
                              'upload': True,
                          })

    def test_no_owner(self):
        self.assertEquals(get_user_task_permissions(self.other_user, self.task),
                          {
                              'write': False,
                              'read': False,
                              'modify': False,
                              'owner': False,
                              'view_task': False,
                              'upload': False,
                          })

    def test_accepted_user(self):
        self.taskOffer = TaskOffer.objects.create(
            task=self.other_task,
            offerer=self.other_profile,
            status='a')
        self.assertEquals(get_user_task_permissions(self.other_user, self.other_task),
                          {
                              'write': True,
                              'read': True,
                              'modify': True,
                              'owner': False,
                              'upload': True,
                          })

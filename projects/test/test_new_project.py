from datetime import datetime, timedelta
import pytz
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

from projects.models import ProjectCategory, Task
from projects.models import Project


class TestCreateProject(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='admin', password='qwerty123')
        self.category = ProjectCategory.objects.create(pk=1, name="testCategory")
        self.c = Client()

    def test_valid_input(self):
        """ Verify that a new project will be created with a valid input """
        self.c.login(username='admin', password='qwerty123')
        test_data = {'title': 'testTitle', 'description': 'testDescription', 'category_id': 1,
                     'task_title': ['testTask'],
                     'task_description': 'testTaskDescription', 'task_budget': 200}
        response = self.c.post(reverse('new_project'), test_data, follow=False)
        self.assertEqual(Project.objects.filter(title='testTitle', description='testDescription').exists(), True)
        self.assertEqual(Task.objects.filter(title='testTask', description='testTaskDescription').exists(), True)
        self.assertEqual(response.status_code, 302)

    def test_no_name(self):
        """ Verify that a new project will not be created without a name """
        self.c.login(username='admin', password='qwerty123')
        test_data = {'description': 'testDescription', 'category_id': 1,
                     'task_title': ['testTask'],
                     'task_description': 'testTaskDescription', 'task_budget': 200}
        response = self.c.post(reverse('new_project'), test_data, follow=False)
        self.assertNotEquals(Project.objects.filter(description='testDescription').exists(), True)
        self.assertNotEquals(Task.objects.filter(description='testTaskDescription').exists(), True)
        self.assertTemplateUsed(response, 'projects/new_project.html')

    def test_no_description(self):
        """ Verify that a new project will not be created without a description """
        self.c.login(username='admin', password='qwerty123')
        test_data = {'title': 'testTitle', 'category_id': 1,
                     'task_title': ['testTask'],
                     'task_description': 'testTaskDescription', 'task_budget': 200}
        response = self.c.post(reverse('new_project'), test_data, follow=False)
        self.assertNotEquals(Project.objects.filter(title='testTitle').exists(), True)
        self.assertNotEquals(Task.objects.filter(title='testTitle').exists(), True)
        self.assertTemplateUsed(response, 'projects/new_project.html')

    def test_access_without_login(self):
        """ Verify that a project can not be created without being authenticated. """
        response = self.c.post(reverse('new_project'), follow=False)
        self.assertIsNot(response.status_code, 200)

    # TODO: This method should also work, however it did not work for the original code either.
    # As so its not relevant now?
    # def test_no_tasks(self):
    #     """ Verify that a new project will not be created without a task """
    #     self.c.login(username='admin', password='qwerty123')
    #     test_data = {'title': 'testTitle','description': 'testDescription', 'category_id': 1}
    #     response = self.c.post(reverse('new_project'), test_data, follow=False)
    #     self.assertEquals(Project.objects.filter(title='testTitle').exists(), True)
    #     self.assertNotEquals(Task.objects.filter(title='testTitle').exists(), True)
    #     self.assertTemplateUsed(response, 'projects/new_project.html')